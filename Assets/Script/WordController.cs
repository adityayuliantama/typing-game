using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WordController : MonoBehaviour
{
    // Start is called before the first frame update
    public int wordSpawned;
    public int wordDestroyed;

    public float spawnTime;
    
    public float specialTime;

    public bool isPaused = false;

    public WordContains wordBase;
    public GameObject wordContainer;
    public GameObject bulletContainer;

    public SpecialController spController;

    public WordContains selectedWord;

    public Transform[] spawnPos;

    public LevelController lvController;

    void Start()
    {
        isPaused = false;
        spawnTime = lvController.spawnDelay;
        specialTime = lvController.specialDelay;
    }

    // Update is called once per frame
    void Update()
    {
        if (!isPaused && spController.moveModifier > 0 && lvController.gameState == "playing")
        {
            if (wordSpawned < lvController.wordContinueSpawn * lvController.gameLevel)
            {
                if (spawnTime < 0)
                {
                    spawnTime += lvController.spawnDelay;
                    spawnWord();
                }
                else
                {
                    spawnTime -= Time.deltaTime;
                }
            }

            if (wordDestroyed == lvController.wordContinueSpawn * lvController.gameLevel)
            {
                wordSpawned = 0;
                wordDestroyed = 0;
                lvController.LevelUp();

            }

            if (specialTime < 0 && lvController.gameState == "playing")
            {
                specialTime += lvController.specialDelay;
                spawnSPWord();
            }
            else
            {
                specialTime -= Time.deltaTime;
            }
        }
    }

    void spawnWord()
    {
        var obj = Instantiate(wordBase, spawnPos[(int)Random.Range(0, spawnPos.Length-1)]);
        WordContains objWord = obj.GetComponent<WordContains>();

        objWord.spCon = spController;
        objWord.level = lvController.gameLevel;
        objWord.type = 0;
        obj.transform.parent = wordContainer.transform;
        objWord.wordCon = this;

        wordSpawned++;


    }

    void spawnSPWord()
    {
        var obj= Instantiate(wordBase, spawnPos[(int)Random.Range(0, spawnPos.Length - 1)]);
        WordContains objWord = obj.GetComponent<WordContains>();
        obj.GetComponent<WordContains>().type = (int)Random.Range(0.5f,3.5f);
        //objWord.type = 1;
        objWord.spCon = spController;
        objWord.level = lvController.gameLevel;
        obj.transform.parent = wordContainer.transform;
        objWord.wordCon = this;
    }

    public void destroyAll()
    {
        WordContains[] all;

        all = wordContainer.GetComponentsInChildren<WordContains>();
        foreach (WordContains child in all)
        {

            destroyBullet(child.transform);
            //GameObject.Destroy(child.gameObject);
            child.destroyTarget();

        }
    }

    public bool checkWord(string check)
    {
        if (selectedWord)
        {
            if (selectedWord.word.getLastWord() == check)
            {
                selectedWord.word.removeLastWord();
                return true;
            }
        }

        else
        {
            getIsSelecting(check);
            return true;
        }
        return false;
    }

    public void getIsSelecting(string check)
    {
        WordContains[] all;

        all = wordContainer.GetComponentsInChildren<WordContains>();

        foreach(WordContains child in all)
        {
            if (!child.word.isDone)
            {
                if (child.word.getLastWord() == check)
                {
                    //Debug.Log("betul");
                    selectedWord = child;
                    child.word.removeLastWord();
                    break;
                }
            }
        }
    }

    public void checkIsWordEmpty()
    {
        if (selectedWord.word.wordLeft.Length == 0 && !selectedWord.word.isDone)
        {
            selectedWord.word.isDone = true;
            selectedWord = null;
        }
    }

    public GameObject getCurrentSelected()
    {
        if(selectedWord != null)
        {
            return selectedWord.gameObject;
        }
        return null;
    }

    public void destroyBullet(Transform obj)
    {
        BulletController[] all;

        all = bulletContainer.GetComponentsInChildren<BulletController>();

        foreach(BulletController child in all)
        {
            if (child.target == obj)
            {
                
                GameObject.Destroy(child.gameObject);
            }

        }
    }

    public void initiateSpecial(int _type)
    {
        spController.startSpecial(_type);
    }
    
    public int checkPaused()
    {
        if (isPaused)
        {
            return 0;
        }
        return 1;
    }
}
