using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class WordContains : MonoBehaviour
{
    // Start is called before the first frame update
    public int level=1;
    public WordsObj word;
    Transform player;
    public TextMeshProUGUI text;
    public int hp;
    public WordController wordCon;
    public SpecialController spCon;
    public float moveSpeed=1.0f;
    public int type;
    
    float adjustX;

    void Start()
    {
        
        //level = (int)Random.Range(1, wordCon.gameLevel);
        player = GameObject.Find("Player").transform;
        word = new WordsObj(level, player,type);
        hp = word.wordValue.Length;
        text = GetComponentsInChildren<TextMeshProUGUI>()[0];
        adjustX = Random.Range(-1.5f, 1.5f);
        moveSpeed = Mathf.Clamp(1f + (0.2f * level),1f,3f);
        //moveSpeed = 2.5f;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 target = player.transform.position;
        transform.position = Vector3.MoveTowards(transform.position, new Vector3(target.x+adjustX,-5.0f,transform.position.z),moveSpeed*spCon.moveModifier*Time.deltaTime);

        setTextColor();
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (word.type == 0)
        {
            player.GetComponent<PlayerController>().substractHp(1);
            wordCon.wordDestroyed++;
        }

        wordCon.destroyBullet(transform);
        GameObject.Destroy(gameObject);
    }

    public void destroyTarget()
    {
        if (type != 0)
        {
            wordCon.initiateSpecial(type);
            
        }
        else
        {
            wordCon.wordDestroyed++;
        }
        GameObject.Destroy(gameObject);
        player.GetComponent<PlayerController>().addScore(word.wordValue.Length);

    }

    public void setTextColor()
    {
        string opening = "<color=red>";
        string closing = "</color>";
        string output="";
        for (int i=0; i < word.wordValue.Length; i++)
        {
            if (i < word.wordValue.Length-hp)
            {
                opening = "<color=white>";
            }
            else
            {
                opening = "<color=red>";
            }
            output += opening;
            output += word.wordValue.Substring(i, 1);
            output += closing;
        }
        text.text = output;

    }
}
