using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    // Start is called before the first frame update
    public int hp=3;
    public float score = 0;
    public float HiScore = 0;
    public Transform bulletContainer;
    public SpecialController spCon;
    public TurretScript turret;
    public LevelController lvCon;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void spawnBullet(GameObject bulletPrefab, GameObject target)
    {
        GameObject obj;
        obj = Instantiate(bulletPrefab, turret.transform);
        obj.transform.parent = bulletContainer;
        obj.GetComponent<BulletController>().target = target.transform;
    }
    public void setHp(int _hp)
    {
        hp = _hp;
    }

    public void substractHp(int _value)
    {
        if (hp > 1)
        {
            hp -= _value;
        }
        else
        {
            lvCon.gameOver();
            
        }
    }

    public void addHp(int _value)
    {
        hp += _value;
    }


    public void setScore(int _score)
    {
        score = _score;
    }

    public void addScore(int _value)
    {
        score += _value*spCon.scoreModifier;
    }
}
