using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour
{
    // Start is called before the first frame update
    public Transform target;
    WordContains targetCon;
    WordController wordCon;
    float moveSpeed = 25.0f;


    void Start()
    {
        transform.localScale = new Vector3(0.2f,0.2f,0.2f);
        targetCon = target.GetComponent<WordContains>();
        wordCon = GameObject.Find("WordController").GetComponent<WordController>();
        
    }


    // Update is called once per frame
    void Update()
    {
        if (Vector3.Distance(transform.position,target.position)>0.2) 
        {
            transform.position = Vector3.MoveTowards(transform.position, target.position, moveSpeed * Time.deltaTime);
        }

        else
        {
            GameObject.Destroy(gameObject);
            if (targetCon.hp > 1)
            {
                targetCon.hp--;
            }
            else
            {
                target.GetComponent<WordContains>().destroyTarget();
            }
        }
    }

}
