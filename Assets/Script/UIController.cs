using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class UIController : MonoBehaviour
{
    // Start is called before the first frame update
    public PlayerController playerController;
    public WordController wordController;
    public LevelController lvController;

    public TextMeshProUGUI scoreText;
    public TextMeshProUGUI hpText;
    public TextMeshProUGUI LvText;

 
    public GameObject menuPanel;
    public GameObject LvPanel;
    public GameObject gameOverPanel;
    public Text gameOverText;

    

    void Start()
    {
        


    }

    // Update is called once per frame
    void Update()
    {
        setScoreText();
        setHpText();
        setTimeScale();


        //checkPause();
    }

    void setScoreText()
    {
        string output = "Score : ";
        output += playerController.score.ToString();
        scoreText.text = output;
    }

    void setHpText()
    {
        string output = "HP : ";
        output += playerController.hp.ToString();
        hpText.text = output;
    }

    void setLvText()
    {
        string output = "Level : ";
        output += lvController.gameLevel;
        LvText.text = output;
    }

    public void showMenu()
    {
        if(wordController.isPaused)
        {
            menuPanel.transform.DOLocalMoveY(1344, 1).SetEase(Ease.InOutExpo).SetUpdate(UpdateType.Late, true);
            wordController.isPaused = false;
            
        }
        else
        {
            menuPanel.transform.DOLocalMoveY(0, 1).SetEase(Ease.InOutExpo).SetUpdate(UpdateType.Late, true);
            wordController.isPaused = true;

        }
        
    }

    public void restartGame()
    {

    }

    public void toMainMenu()
    {
        lvController.levelStart();
    }

    public void lvPopUp()
    {
        LvPanel.transform.localPosition = new Vector3(-1600, 480, 0);
        //LvPanel.transform.DOLocalMoveX(0, 1).SetEase(Ease.InOutExpo).SetUpdate(true);
        Sequence LvPanelSeq = DOTween.Sequence();

        LvPanelSeq.Append(LvPanel.transform.DOLocalMoveX(0, 1).SetEase(Ease.InOutExpo));

        LvPanelSeq.Append(LvPanel.transform.DOLocalMoveX(1600, 1).SetEase(Ease.InOutBack).SetDelay(2));

        LvPanelSeq.OnComplete(PopUpEnd);

        LvPanelSeq.PrependInterval(0.2f);

    }

    public void gameOverPopUp()
    {
        if (PlayerPrefs.GetFloat("HiScore") < playerController.score)
        {
            PlayerPrefs.SetFloat("HiScore", playerController.score);
        }
        wordController.isPaused = true;
        gameOverText.text = "Score : "+playerController.score+"\nHigh Score : "+ PlayerPrefs.GetFloat("HiScore") + ((playerController.HiScore<playerController.score)?"\n\nNew High Score!!":"");
        gameOverPanel.transform.position = new Vector3(0, -1311, 0);
        gameOverPanel.transform.DOLocalMoveY(0, 1.5f).SetEase(Ease.InOutExpo).SetUpdate(UpdateType.Late, true);
    }

    public void PopUpEnd()
    {
        lvController.levelStart();
    }
    void setTimeScale()
    {
        if (wordController.isPaused)
        {
            Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = 1;
        }
    }
}
