using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretScript : MonoBehaviour
{

    public WordController wordCon;
    void Start()
    {
        wordCon = GameObject.Find("WordController").GetComponent<WordController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (wordCon.selectedWord != null)
        {
            Vector3 target = wordCon.selectedWord.transform.position;
            float angle = Mathf.Atan2(target.y-transform.position.y, target.x-transform.position.x) * Mathf.Rad2Deg;
            Quaternion targetRotation = Quaternion.AngleAxis(angle, Vector3.forward);
            transform.localRotation = Quaternion.Slerp(transform.rotation, targetRotation, 10.0f * Time.deltaTime);



        }
        else
        {
            //targetRotation = new Quaternion(0, 0, 0, 0);
        }
        
    }
}
