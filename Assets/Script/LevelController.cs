using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelController : MonoBehaviour
{
    // Start is called before the first frame update
    public int gameLevel = 1;
    public float spawnDelayDefault = 2.0f;
    public float spawnDelay = 2.0f;
    public float specialDelay = 18.0f;

    public int wordContinueSpawn=25;

    public string gameState = "playing";

    public UIController UIController;
    

    void Start()
    {
        levelStart();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void LevelUp()
    {
        gameLevel++;
        gameState = "LvUp";
        UIController.lvPopUp();
    }

    public void levelStart()
    {
        gameState = "playing";
        spawnDelay = Mathf.Clamp(spawnDelayDefault - (gameLevel - 1) * 0.05f, 1f, spawnDelay);
        //Debug.Log("mula");

    }

    public void gameOver()
    {
        
        UIController.gameOverPopUp();
    }
}
