using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TypeController : MonoBehaviour
{
    // Start is called before the first frame update
    public string a;
    WordController wordCon;
    public GameObject bulletPrefab;
    public PlayerController playerCon;
    void Start()
    {
        wordCon = GetComponent<WordController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.anyKeyDown && !wordCon.isPaused && !(Input.GetMouseButtonDown(0)
            || Input.GetMouseButtonDown(1) || Input.GetMouseButtonDown(2)))
        {
            keyPressed();
        }
    }

    void keyPressed()
    {
        if (Input.inputString.Length == 1)
        {
            a = Input.inputString;

            if (wordCon.checkWord(a) && wordCon.getCurrentSelected() != null)
            {
                playerCon.spawnBullet(bulletPrefab,wordCon.getCurrentSelected());
                wordCon.checkIsWordEmpty();
            }
            
        }
    }
}
