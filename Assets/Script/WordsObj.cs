using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class WordsObj
{
    // Start is called before the first frame update
    public string wordValue;
    public string wordLeft="";
    public Transform player;
    public bool isDone;
    public int type;

    
    

    public WordsObj(int level, Transform _player, int _type)
    {
        type = _type;
        player = _player;
        wordValue = selectWord(level);
        wordLeft = wordValue;
    }



    public string selectWord(int level)
    {
        string value="";
        
        

        if (type == 0)
        {
            string[,] wordArray = new string[,] {
            { "apa", "itu", "era", "ubi", "ibu", "ria","dia" , "bau" , "ego","oke"},
            { "kata","kita","raba","tali","luar","anak","untuk", "laku", "fana","gaun" },
            { "tidak","lihat","hutan","lalat","busuk","menit","batuk" , "katup", "letak","jatuh" },
            { "kering","kepala","rambut","tanduk","lengan","ganggu","jingga","anggur" , "delima","kantuk"},
            { "rahasia", "terbuka" , "berlaku","ranting","teratai","hanggar","pesawat","kembali","canggung", "berlaku"}
            };
            int selectedIndex = Random.Range(0, 9);

            int levelIndex = (int)Random.Range(0,Mathf.Clamp(level - 1, 0, 4));

            //Debug.Log(levelIndex);

            value = wordArray[levelIndex, selectedIndex];
        }

        else
        {
            string[] spWordArray = new string[]
            {
            "#zawarudo","#multiplier","#repair"
            };
            value = spWordArray[type-1];
        }
        return value;
    }

    public string getLastWord()
    {
        string last;

        last = wordLeft.Substring(0,1);

        return last;
    }

    public void removeLastWord()
    {
        wordLeft = wordLeft.Substring(1, wordLeft.Length - 1);
    }
}