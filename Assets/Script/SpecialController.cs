using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpecialController : MonoBehaviour
{
    public float maxSpecialTime = 4.0f;
    public float specialTime;
    public bool isInEffect;
    public int currentSPEffect;
    public float moveModifier=1.0f;
    public float scoreModifier = 1.0f;
    public int hpAdd=2;

    public PlayerController player;

    public WordController wordCon;

    AudioSource audioSource;
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (specialTime > 0)
        {
            specialTime -= Time.deltaTime;
            if (specialTime < maxSpecialTime)
            {
                applyEffect();
            }
            
        }
        else
        {
            if (isInEffect)
            {
                endSpecial();
            }
        }
    }

    public void startSpecial(int _type)
    {
        currentSPEffect = _type;
        if (_type == 1)
        {
            //Debug.Log("zawarudo");
            audioSource.Play();
            specialTime += 3;
        }
        if (_type == 3)
        {
            player.addHp(hpAdd);
        }

        else
        {
            isInEffect = true;
            specialTime += maxSpecialTime;
        }
    }

    void applyEffect()
    {
        if (currentSPEffect == 1)
        {
            moveModifier = 0;
        }
        else if (currentSPEffect == 2)
        {
            scoreModifier = 2;
        }
        
    }

    public void endSpecial()
    {
        moveModifier = 1;
        scoreModifier = 1;
        isInEffect = false;
    }
}
